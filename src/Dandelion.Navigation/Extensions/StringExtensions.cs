﻿using System;
using System.Globalization;

namespace Dandelion.Navigation.Extensions
{
    public static class StringExtensions
    {
        public static string SubstringAfter(this string s, char c)
        {
            var index = s.IndexOf(c);
            return index >= s.Length - 1 ? "" : s.Substring(index + 1);
        }
        public static string SubstringBefore(this string s, char c)
        {
            var index = s.IndexOf(c);
            return index == -1 ? s : s.Remove(index);
        }

        public static string SubstringAfter(this string s, string str, StringComparison comparison)
        {
            var index = s.IndexOf(str, comparison);
            return index >= s.Length - str.Length ? "" : s.Substring(index + str.Length);
        }
    }
}