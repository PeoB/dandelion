﻿using System;

namespace Dandelion.Navigation.Extensions
{
    public static class IntegerExtensions
    {
        public static void ForEach(this int times, Action<int> action)
        {
            for (var i = 0; i < times; ++i)
                action(i);
        }
    }
}