﻿using System;

namespace Dandelion.Navigation.Extensions
{
    public static class TypeExtensions
    {
        public static string AssemblyName(this Type type)
        {
            return type.Assembly.FullName.SubstringBefore(',');
        }
    }
}