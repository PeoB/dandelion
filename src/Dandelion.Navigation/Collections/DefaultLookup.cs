﻿using System;
using System.Collections.Generic;

namespace Dandelion.Navigation.Collections
{
    public class DefaultLookup<T, T2> : DefaultDictionary<T, ICollection<T2>>
    {
        public override ICollection<T2> this[T key]
        {
            get
            {
                if (!ContainsKey(key))
                    base[key] = new List<T2>();
                return base[key];
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}