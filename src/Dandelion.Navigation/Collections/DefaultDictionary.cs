using System;
using System.Collections;
using System.Collections.Generic;

namespace Dandelion.Navigation.Collections
{
    public class DefaultDictionary<T, T1> : IDictionary<T, T1>
    {
        private readonly IDictionary<T, T1> _dictionary = new Dictionary<T, T1>();
        private readonly Func<T, T1> _defaultValue = t => default(T1);

        public DefaultDictionary() { }
        public DefaultDictionary(T1 defaultValue) : this(t => defaultValue) { }
        public DefaultDictionary(Func<T, T1> defaultValue) { _defaultValue = defaultValue; }

        public IEnumerator<KeyValuePair<T, T1>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(KeyValuePair<T, T1> item)
        {
            _dictionary.Add(item);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        public bool Contains(KeyValuePair<T, T1> item)
        {
            return _dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<T, T1>[] array, int arrayIndex)
        {
            _dictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<T, T1> item)
        {
            return _dictionary.Remove(item);
        }

        public int Count
        {
            get { return _dictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return _dictionary.IsReadOnly; }
        }

        public bool ContainsKey(T key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void Add(T key, T1 value)
        {
            _dictionary.Add(key, value);
        }

        public bool Remove(T key)
        {
            return _dictionary.Remove(key);
        }

        public bool TryGetValue(T key, out T1 value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public virtual T1 this[T key]
        {
            get
            {
                return !ContainsKey(key) ? _defaultValue(key) : _dictionary[key];
            }
            set { _dictionary[key] = value; }
        }

        public ICollection<T> Keys
        {
            get { return _dictionary.Keys; }
        }

        public ICollection<T1> Values
        {
            get { return _dictionary.Values; }
        }
    }
}