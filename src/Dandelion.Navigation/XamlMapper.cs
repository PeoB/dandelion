﻿using System;
using System.Reflection;
using System.Windows;
using Microsoft.Phone.Controls;
using Dandelion.Navigation.Extensions;

namespace Dandelion.Navigation
{
    class XamlMapper
    {
        public static string PathFor<T>() where T : PhoneApplicationPage
        {
            var type = typeof(T);
            return
                string.Format("/{0};component{1}.xaml",
                              type.AssemblyName(),
                              type.FullName.SubstringAfter(type.AssemblyName(), StringComparison.Ordinal).Replace('.', '/'));
        }

        public static Type TypeFromPath(Uri source)
        {
            var baseAssemblyName = Application.Current.GetType().AssemblyName();
            var t = Type.GetType(baseAssemblyName + source.OriginalString.SubstringBefore('.').Replace('/', '.'));
            return t;
        }

        public static Uri UriFor(Type type)
        {
            return new Uri(string.Format("/{0};component{1}.xaml",
                              type.AssemblyName(),
                              type.FullName.SubstringAfter(type.AssemblyName(), StringComparison.Ordinal).Replace('.', '/')));
        }

        public static bool IsSame(Type type, Uri source)
        {
            var path = source.OriginalString.SubstringBefore('?');
            path = path.Contains(";") ? path : string.Format("/{0};component{1}", Application.Current.GetType().AssemblyName(), path);
            return UriFor(type).LocalPath == path;
        }
    }
}