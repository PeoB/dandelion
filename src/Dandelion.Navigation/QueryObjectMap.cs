﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Dandelion.Factory.Extensions;
using Dandelion.Navigation.Collections;
using Dandelion.Navigation.Extensions;

namespace Dandelion.Navigation
{
    public class QueryObjectMap
    {
        private static readonly QueryObjectMap _instance = new QueryObjectMap();
        private readonly IDictionary<Type, string> _typeMappings = new DefaultDictionary<Type, string>(t => t.FullName);
        private QueryObjectMap()
        {
            //TODO, load from isolated storage
        }

        public static QueryObjectMap Instance { get { return _instance; } }

        public void Seed(Dictionary<Type, string> typeMappings)
        {
            if (_typeMappings.Values.Intersect(typeMappings.Values).Any())
            {
                throw new AmbiguousMatchException("Type query key already used");
            }
            typeMappings.ForEach(_typeMappings.Add);
        }

        public string QueryFrom<T>() where T : ISeedable
        {
            var typeOfT = typeof(T);
            if (!_typeMappings.ContainsKey(typeOfT))
                _typeMappings[typeOfT] = typeOfT.Name; //TODO: handle duplicate ID names
            return _typeMappings[typeOfT];
        }

        public Type TypeFromQuery(string parameterName)
        {
            //TODO, handle not found types by having a double sided default dictionary
            return _typeMappings.First(t => t.Value == parameterName).Key;
        }
    }
}