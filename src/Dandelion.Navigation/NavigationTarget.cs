﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using Dandelion.Factory.Extensions;
using Microsoft.Phone.Controls;
using Dandelion.Navigation.Extensions;

namespace Dandelion.Navigation
{
    public class NavigationTarget<T> where T : PhoneApplicationPage
    {
        private readonly string _uri;
        private readonly Dictionary<string, ISeedable> _seedables = new Dictionary<string, ISeedable>();
        private readonly Dictionary<string, string> _seedableIds = new Dictionary<string, string>();
        private readonly QueryObjectMap _queryObjectMap = QueryObjectMap.Instance;
        private readonly List<Action<T>> _actions = new List<Action<T>>();
        public NavigationTarget() : this(XamlMapper.PathFor<T>()) { }
        public NavigationTarget(string uri)
        {
            _uri = uri;
        }

        public IEnumerable<Action<T>> Actions { get { return _actions; } }

        public NavigationTarget<T> With<T2>(T2 obj, Expression<Func<T, T2>> expression) where T2 : ISeedable
        {
            var assignment = (MemberExpression)expression.Body;
            if (assignment.Member.DeclaringType != typeof(T) || assignment.Member.MemberType != MemberTypes.Property) throw new ArgumentException("Only properties on the navigationtarget may be used");
            var setter = ((PropertyInfo)assignment.Member).GetSetMethod();

            _actions.Add(page => setter.Invoke(page, new object[] { obj }));

            return With(obj);
        }

        public NavigationTarget<T> With<T2>(T2 obj, Action<T, T2> action) where T2 : ISeedable
        {
            _actions.Add(page => action(page, obj));
            return With(obj);
        }

        public NavigationTarget<T> With<T2>(T2 obj) where T2 : ISeedable
        {
            _seedables.Add(_queryObjectMap.QueryFrom<T2>(), obj);
            return this;
        }

        public NavigationTarget<T> With<T2>(string seed) where T2 : ISeedable
        {
            _seedableIds.Add(_queryObjectMap.QueryFrom<T2>(), seed);
            return this;
        }

        public NavigationTarget<T> Executing(Action<T> action)
        {
            _actions.Add(action);
            return this;
        }
        public NavigationTarget<T> AsBackNavigation()
        {
            ClearBackstackUntil = typeof(T);
            Inclusive = true;
            return this;
        }

        public NavigationTarget<T> RemoveBackEntriesUntil<T2>(ClearType clearType)
        {
            ClearBackstackUntil = typeof(T2);
            Inclusive = clearType == ClearType.Inclusive;
            return this;
        }

        internal Type ClearBackstackUntil { get; set; }
        internal bool Inclusive { get; set; }
        public void Now()
        {
            //TODO, work against a class that contains all the data about the navigation
            NavigationResults.Instance.NavigatingWith(_seedables, Actions);
            NavigationProcessor.NavigateTo(this);
        }

        public string DeepLink()
        {
            return _uri + "?" + _seedables
                .Select(kv => kv.Key + "=" + HttpUtility.UrlEncode(kv.Value.Seed))
                .Union(_seedableIds.Select(kv => kv.Key + "=" + HttpUtility.UrlEncode(kv.Value)))
                .Join("&");
        }
    }
    public enum ClearType
    {
        Inclusive, Exclusive
    }
}
