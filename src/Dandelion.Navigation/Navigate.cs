﻿using Microsoft.Phone.Controls;

namespace Dandelion.Navigation
{
    public class Navigate
    {
        public static NavigationTarget<T> To<T>() where T : PhoneApplicationPage
        {
            return new NavigationTarget<T>();
        }

        public static NavigationTarget<PhoneApplicationPage> To(string uri)
        {
            return new NavigationTarget<PhoneApplicationPage>(uri);
        }

        public static void Back()
        {

        }
    }
}