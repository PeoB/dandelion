﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dandelion.Factory;
using Dandelion.Factory.Extensions;
using Dandelion.Navigation.Collections;
using Dandelion.Navigation.Extensions;
using Microsoft.Phone.Controls;

namespace Dandelion.Navigation
{
    public class NavigationResults
    {
        private static readonly NavigationResults _instance = new NavigationResults();
        private NavigationResults() { }
        public static NavigationResults Instance { get { return _instance; } }

        private Dictionary<string, ISeedable> _seedables;
        private readonly IDictionary<string, string> _queryString = new DefaultDictionary<string, string>();
        private string _query;
        private IEnumerable<Action<PhoneApplicationPage>> _actions;

        public string Query
        {
            get { return _query; }
            set
            {
                _query = value;
                _queryString.Clear();
                _query.Split('&')
                    .Select(s => s.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries)).Where(s => s.Count() == 2) //TODO, add support for having = in the value
                    .ForEach(s => _queryString[s[0]] = s[1]); //TODO, handle several values that are the same(arrays)
            }
        }

        public void Object<T>(Action<T> fullyGrown) where T : ISeedable
        {
            var seedable = (_seedables.Select(s => s.Value).FirstOrDefault(s => s is T));
            if (seedable != null)
            {
                fullyGrown((T)seedable);
                return;
            }
            PlantSchool.Grow<T>().From(SeedFor<T>()).Now(fullyGrown);
        }
        private string SeedFor<T>() where T : ISeedable
        {
            return _queryString[QueryObjectMap.Instance.QueryFrom<T>()];
        }

        public IEnumerable<T> Objects<T>() where T : ISeedable
        {
            return Enumerable.Empty<T>();
        }

        //TODO: should this not be in a more factoryish class?
        public void Objects<T>(string seed, Action<IEnumerable<T>> allFullyGrown) where T : ISeedable
        {
            allFullyGrown(Enumerable.Empty<T>());
        }

        public string String(string key)
        {
            return _queryString[key];
        }

        public void NavigatingWith<T>(Dictionary<string, ISeedable> seedables, IEnumerable<Action<T>> actions) where T : PhoneApplicationPage
        {

            _actions = actions.Select<Action<T>, Action<PhoneApplicationPage>>(a => (page => a((T)page)));
            _seedables = seedables;
        }

        public void NavigationDone(PhoneApplicationPage page)
        {
            _actions.ForEach(a => a(page));
            _actions = Enumerable.Empty<Action<PhoneApplicationPage>>();
        }


    }
}