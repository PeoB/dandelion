﻿using System;
using System.Windows.Navigation;
using Dandelion.Factory.Extensions;
using Dandelion.Navigation.Extensions;
using Microsoft.Phone.Controls;
using System.Linq;

namespace Dandelion.Navigation
{
    public static class NavigationProcessor
    {
        private static PhoneApplicationFrame _rootFrame;
        private static bool _inclusive;

        public static PhoneApplicationFrame RootFrame
        {
            get { return _rootFrame; }
            set
            {
                if (_rootFrame != null) throw new InvalidOperationException("Root frame is already set");
                _rootFrame = value;
                _rootFrame.Navigated += RootFrameNavigated;
                //TODO handle failed navigations
            }
        }

        private static void RootFrameNavigated(object sender, NavigationEventArgs e)
        {
            if (e.Content == null) return;
            HandleBackStack();
            NavigationResults.Instance.Query = e.Uri.OriginalString.SubstringAfter('?');
            NavigationResults.Instance.NavigationDone((PhoneApplicationPage)e.Content);
        }

        private static void HandleBackStack()
        {
            if (ClearBackstackUntil == null) return;
            var numBackEntriesToRemove = _rootFrame.BackStack.IndexOf(entry =>
                XamlMapper.IsSame(ClearBackstackUntil, entry.Source));
            if (_inclusive) numBackEntriesToRemove++;
            numBackEntriesToRemove.ForEach(i => _rootFrame.RemoveBackEntry());

            ClearBackstackUntil = null;
        }

        public static void NavigateTo<T>(NavigationTarget<T> target) where T : PhoneApplicationPage
        {
            ClearBackstackUntil = target.ClearBackstackUntil;
            _inclusive = target.Inclusive;
            RootFrame.Navigate(new Uri(target.DeepLink(), UriKind.Relative));
        }

        private static Type ClearBackstackUntil { get; set; }
    }
}