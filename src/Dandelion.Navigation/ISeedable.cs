using System;
using System.Collections.Generic;
using System.Linq;

namespace Dandelion.Navigation
{
    public interface ISeedable
    {
        string Seed { get; }
    }

    public interface ISeedablePlantSchool<T> where T : ISeedable
    {
        bool Plant(string seed, Action<T> fullyGrown);
        bool PlantMany(string seed, Action<IEnumerable<T>> fullyGrown);
    }

    public interface ISeedablePlantSchool
    {
        Type[] SupportedTypes { get; }
        bool Plant<T>(string seed, Action<T> fullyGrown) where T : ISeedable;
        bool Plant<T>(string seed, Action<IEnumerable<T>> fullyGrown) where T : ISeedable;
    }

    class SeedablePlantSchool : ISeedablePlantSchool
    {
        private readonly Func<string, Action<object>, bool> _plant;
        private readonly Func<string, Action<IEnumerable<object>>, bool> _plants;

        public static SeedablePlantSchool Build<T>(ISeedablePlantSchool<T> school) where T : ISeedable
        {
            var plantSchool = new SeedablePlantSchool(
                (s, a) => school.Plant(s, o => a(o)),
                (s, a) => school.PlantMany(s, o => a(o.Cast<object>()))) { SupportedTypes = new[] { typeof(T) } };

            return plantSchool;
        }

        private SeedablePlantSchool(Func<string, Action<object>, bool> plant, Func<string, Action<IEnumerable<object>>, bool> plants)
        {
            _plant = plant;
            _plants = plants;
        }

        public Type[] SupportedTypes { get; private set; }

        public bool Plant<T>(string seed, Action<T> fullyGrown) where T : ISeedable
        {
            return _plant(seed, t => fullyGrown((T)t));
        }

        public bool Plant<T>(string seed, Action<IEnumerable<T>> fullyGrown) where T : ISeedable
        {
            return _plants(seed, t => fullyGrown(t.Cast<T>()));
        }
    }

}